
// let test = require("test");
// console.log(test);
// function start(){
//     console.log("test");
// }
// console.log(module);
var http = require("http");

function connect() {
    
    var server = new http.Server();
    server.listen(1337, "127.0.0.1");
    server.on("request", function (req, res) {
        console.log(req.url)
        if(req.url=="/about")
        {
           res.setHeader('Content-Type', 'text/html');
            res.write("about");
        }
        res.end();
    })
}


if (module.parent){
    exports.connect = connect;
}
else {
    connect();
}